
--[[

 'hint' mod - for Inside The Box.

]]--

minetest.register_on_chat_message(function(name, message)
	if message:sub(1, 1) == "/" then
		return
	end
	if message == "hint" then
		minetest.chat_send_player(name, minetest.colorize("#44ff44",
			"Just so you know, there are videos showing how to do the tutorial on our website!"))
		return true
	end
end)

